import buble from '@rollup/plugin-buble'; // Transpile/polyfill with reasonable browser support
import commonjs from '@rollup/plugin-commonjs'; // Convert CommonJS modules to ES6
import vue3plugin from 'rollup-plugin-vue'; // Handle .vue SFC files
import vue2plugin from 'rollup-plugin-vue2'; // Handle .vue SFC files
import postcss from 'rollup-plugin-postcss';

const vue2 = process.env.ROLLUP_VUE_VERSION === '2';

export default {
  input: 'components/index.js', // Path relative to package.json
  output: {
    name: 'SharedUi',
    exports: 'named',
  },
  plugins: vue2 ? [
    commonjs(),
    vue2plugin({
      css: true, // Dynamically inject css as a <style> tag
      compileTemplate: true, // Explicitly convert template to render function
    }),
    buble({
      objectAssign: 'Object.assign',
    }), // Transpile to ES5
  ] : [
    vue3plugin({
      css: false,
      preprocessStyles: true,
      compileTemplate: true, // Explicitly convert template to render function
    }),
    postcss({
      extract: false,
    }),
    commonjs(),
    buble({
      objectAssign: 'Object.assign',
    }), // Transpile to ES5
  ],
};
