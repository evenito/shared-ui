export default [
  {
    id: 'networking',
    title: 'shared.resources.eventType.networking',
    color: '#BA68C8',
  },
  {
    id: 'food',
    title: 'shared.resources.eventType.food',
    color: '#4FC3F7',
  },
  {
    id: 'workshop',
    title: 'shared.resources.eventType.workshop',
    color: '#DCE775',
  },
  {
    id: 'speech',
    title: 'shared.resources.eventType.speech',
    color: '#FFB74D',
  },
];
