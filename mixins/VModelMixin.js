function generate(required = true, propName = 'value', eventName = 'input') {
  return {
    props: {
      [propName]: {
        required,
      },
    },
    computed: {
      vModel: {
        get: function() {
          return this[propName];
        },
        set: function(newValue) {
          this.$emit(eventName, newValue);
        },
      },
    },
  };
}

export const VModelMixin = generate();
export const VModelMixinOptional = generate(false);
export default VModelMixin;

export const VModelMixin3 = generate(true, 'modelValue', 'update:modelValue');
export const VModelMixin3Optional = generate(false, 'modelValue', 'update:modelValue');
