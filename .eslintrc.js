module.exports = {
  root: true,
  env: {
    node: true,
    jest: true,
  },
  parserOptions: {
    parser: 'babel-eslint',
  },
  extends: [
    'prettier',
    'plugin:vue/recommended',
  ],
  plugins: [
    'vue',
    'prettier',
  ],
  rules: {
    'no-console': process.env.VUE_APP_ENVIRONMENT === 'local'
      ? 'off'
      : ['error', { allow: ['warn', 'error'] }],
    'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'warn',
    'comma-dangle': ['warn', 'always-multiline'],
    'semi': ['warn', 'always', { 'omitLastInOneLineBlock': true }],
    'keyword-spacing': ['warn', { 'before': true }],
    'space-before-blocks': 'warn',
    'space-before-function-paren': ['warn', 'never'],
    'comma-spacing': ['warn', { 'before': false, 'after': true }],
    'arrow-spacing': ['warn', { 'before': true, 'after': true }],
    'arrow-parens': ['warn', 'as-needed'],
    'key-spacing': ['warn', {
      singleLine: {
        beforeColon: false,
        afterColon: true,
      },
      multiLine: {
        beforeColon: false,
        afterColon: true,
      },
    }],
    // 'array-bracket-newline': ['warn', 'consistent'],
    // 'array-element-newline': ['warn', 'consistent'],
    // 'object-curly-newline': ['warn', { ObjectExpression: { multiline: true }}],
    // 'object-property-newline': ['warn', { 'allowAllPropertiesOnSameLine': true }],
    // 'padded-object': Doesn't exist https://github.com/eslint/eslint/issues/9048
    'object-curly-spacing': ['warn', 'always', { 'objectsInObjects': false }],
    'brace-style': ['warn', '1tbs', { 'allowSingleLine': true }],
    'quotes': ['warn', 'single', 'avoid-escape'],
    'vue/singleline-html-element-content-newline': ['warn', {
      ignoreWhenNoAttributes: true,
      ignoreWhenEmpty: true,
      ignores: ['v-icon', 'ev-icon'],
    }],
    'vue/max-attributes-per-line': ['warn', {
      'singleline': 3,
      'multiline': {
        'max': 1,
        'allowFirstLine': false,
      },
    }],
    indent: ['warn', 2, {
      flatTernaryExpressions: true,
      offsetTernaryExpressions: false,
      ignoredNodes: ['ConditionalExpression > ConditionalExpression'],
    }],
    'vue/script-indent': ['warn', 2, { 'baseIndent': 1, ignores: [
      // nested objects, excluding top level of exported object (data, methods, computed, etc.)
      // "[value.type='ObjectExpression']:not(:matches(ExportDefaultDeclaration, [left.property.name='exports']) > * > [value.type='ObjectExpression'])",
      // nested arrays
      "[value.type='ArrayExpression']",
    ] }],
    'vue/no-mutating-props': ['warn'], // TODO:  remove once https://github.com/vuejs/eslint-plugin-vue/issues/1371 will be resolved
    'vue/no-arrow-functions-in-watch': ['warn'],
    'vue/valid-v-slot': ['warn'], // TODO: remove once https://github.com/vuejs/eslint-plugin-vue/issues/1229 will be resolved
    'vue/v-slot-style': ['error', {
      'atComponent': 'shorthand',
    }],
    'vue/component-name-in-template-casing': ['error', 'kebab-case', {
      registeredComponentsOnly: false,
      ignores: [],
    }],
    'vue/no-useless-v-bind': ['warn', { ignoreIncludesComment: true }],
    'vue/no-unused-components': ['warn', { 'ignoreWhenBindingPresent': false }],
    'vue/padding-line-between-blocks': ['warn', 'always'],
    'vue/v-for-delimiter-style': ['error', 'in'],
    'vue/no-v-model-argument': 'warn',
  },
  overrides: [
    {
      files: ['*.vue', '*.rawvue'],
      rules: {
        indent: 'off',
        'vue/comment-directive': 'off',
      },

    },
  ],
};
