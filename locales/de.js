export default {
  "entities": {
    "contacts": {
      "statuses": {
        "declined": "Abgemeldet",
        "registered": "Angemeldet",
        "selected": "Ausstehend"
      },
      "translatableFieldTitles": {
        "connect_url": "Persönlicher evenito connect Link des Kontakts zum Einloggen",
        "qr": "QR-Code",
        "registrationSummary": "Registrierungs-Zusammenfassung ",
        "status": "Status",
        "token": "Bitte geben Sie Ihren Anmelde-Token ein:"
      }
    },
    "events": {
      "translatableFieldTitles": {
        "default_language": "Standard-Sprache",
        "description": "Beschreibung",
        "end_timestamp": "Ende",
        "languages": "Sprachen",
        "name": "Name",
        "start_timestamp": "Start",
        "startDateWithDay": "Start-Datum mit Wochentag",
        "startTime": "Startzeit"
      }
    },
    "messageJobs": {
      "translatableFieldTitles": {
        "renderURL": "Link zum Anzeigen der Nachricht im Browser"
      }
    }
  },
  "placeholderSectionTitles": {
    "contactInformation": "Kontaktinformationen",
    "eventInformation": "Eventinformationen",
    "links": "Links",
    "registrationInformation": "Anmeldeinformationen"
  },
  "shared": {
    "resources": {
      "eventType": {
        "food": "Food & Drinks",
        "networking": "Networking",
        "speech": "Vortrag",
        "workshop": "Workshop"
      }
    }
  }
};