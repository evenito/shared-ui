import {reassemble} from '../utils/objectUtils';

import {default as dev} from './dev';
import {default as en} from './en';
import {default as de} from './de';
import {default as fr} from './fr';
import {default as it} from './it';

export const languages = {
  en: {
    value: 'en',
    subCode: 'en-US',
    aliases: ['en', 'eng'],
    strings: en,
  },
  fr: {
    value: 'fr',
    subCode: 'fr-FR',
    aliases: ['fr', 'fre', 'fra'],
    strings: fr,
  },
  de: {
    value: 'de',
    subCode: 'de-DE',
    aliases: ['de', 'deu', 'ger'],
    strings: de,
  },
  it: {
    value: 'it',
    subCode: 'it-IT',
    aliases: ['it', 'ita'],
    strings: it,
  },
};

export const getMessages = clientConfig => {
  return {
    ...reassemble(languages, (lang, declaration) => ({
      [lang]: {
        ...declaration.strings,
        ...clientConfig[lang]
      },
    })),
    dev: {
      ...dev,
      ...clientConfig['dev'],
    }
  };
}
