export default {
  "entities": {
    "contacts": {
      "statuses": {
        "declined": "Non-souscrit",
        "registered": "Enregistré",
        "selected": "Enregistré"
      },
      "translatableFieldTitles": {
        "connect_url": "Lien personnel evenito connect du contact pour la connexion.",
        "qr": "Code QR",
        "registrationSummary": "Résumé de l'enregistrement",
        "status": "Statut",
        "token": "Veuillez entrer le token ici."
      }
    },
    "events": {
      "translatableFieldTitles": {
        "default_language": "Langue par défaut",
        "description": "Description",
        "end_timestamp": "Fin",
        "languages": "Langues",
        "name": "Nom",
        "start_timestamp": "Début",
        "startTime": "Heure de début"
      }
    },
    "messageJobs": {
      "translatableFieldTitles": {
        "renderURL": "Lien pour afficher le message dans le navigateur"
      }
    }
  },
  "placeholderSectionTitles": {
    "contactInformation": "Informations de contact",
    "eventInformation": "Informations sur l'événement",
    "links": "Liens",
    "registrationInformation": "Informations sur l'inscription"
  },
  "shared": {
    "resources": {
      "eventType": {
        "food": "Alimentation",
        "networking": "Networking",
        "speech": "Séminaire",
        "workshop": "Workshop"
      }
    }
  }
};