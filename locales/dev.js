export default {
  shared: {
    resources: {
      eventType: {
        networking: 'Networking',
        food: 'Food',
        workshop: 'Workshop',
        speech: 'Speech',
      },
    },
  },
};
