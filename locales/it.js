export default {
  "entities": {
    "contacts": {
      "statuses": {
        "declined": "Non iscritto",
        "registered": "Iscritto",
        "selected": "Iscritto"
      },
      "translatableFieldTitles": {
        "connect_url": "Link personale di evenito connect del contatto per il log-in",
        "qr": "Codice QR",
        "registrationSummary": "Riepilogo della registrazione",
        "status": "Stato",
        "token": "Si prega di inserire il token qui."
      }
    },
    "events": {
      "translatableFieldTitles": {
        "default_language": "Lingua predefinita",
        "description": "Descrizione",
        "end_timestamp": "Fine",
        "languages": "Lingue",
        "name": "Nome",
        "start_timestamp": "Iniziare",
        "startTime": "Ora di inizio"
      }
    },
    "messageJobs": {
      "translatableFieldTitles": {
        "renderURL": "Link per visualizzare il messaggio nel browser"
      }
    }
  },
  "placeholderSectionTitles": {
    "contactInformation": "Informazioni di contatto",
    "eventInformation": "Informazioni sull'evento",
    "links": "Link",
    "registrationInformation": "Informazioni per la registrazione"
  },
  "shared": {
    "resources": {
      "eventType": {
        "food": "Alimenti",
        "networking": "Networking",
        "speech": "Lecture",
        "workshop": "Workshop"
      }
    }
  }
};