export default {
  "entities": {
    "contacts": {
      "statuses": {
        "declined": "Declined",
        "registered": "Registered",
        "selected": "Pending"
      },
      "translatableFieldTitles": {
        "connect_url": "Contact's personal evenito connect link for log-in",
        "qr": "QR Code",
        "registrationSummary": "Registration summary",
        "status": "Status",
        "token": "Please enter token here."
      }
    },
    "events": {
      "translatableFieldTitles": {
        "default_language": "Default language",
        "description": "Description",
        "end_timestamp": "End",
        "languages": "Languages",
        "name": "Name",
        "start_timestamp": "Start",
        "startDateWithDay": "Start date with weekday",
        "startTime": "Start time"
      }
    },
    "messageJobs": {
      "translatableFieldTitles": {
        "renderURL": "Link to view message in browser"
      }
    }
  },
  "placeholderSectionTitles": {
    "contactInformation": "Contact information",
    "eventInformation": "Event information",
    "links": "Links",
    "registrationInformation": "Registration information"
  },
  "shared": {
    "resources": {
      "eventType": {
        "food": "Food & Drinks",
        "networking": "Networking",
        "speech": "Speech",
        "workshop": "Workshop"
      }
    }
  }
};