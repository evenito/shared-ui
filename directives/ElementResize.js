import { pick, get } from 'lodash';
import { ResizeObserver as Polyfill } from '@juggle/resize-observer';

const ResizeObserver = window.ResizeObserver || Polyfill;
const firstOrSelf = what => (what && what[0]) || what;

const initialize = function(el, binding, vnode) {
  const f = binding.value;
  new ResizeObserver((entries) => {
    const vContent = get(entries, '[0].contentRect');
    const vBox = firstOrSelf(get(entries, '[0].borderBoxSize')); // Chrome provides the array, Firefox provides object
    let box;
    if (vBox) {
      box = { width: vBox.inlineSize, height: vBox.blockSize };
    } else { // Safari doesn't provide borderBoxSize
      const style = window && window.getComputedStyle(get(entries, '[0].target'));
      if (style) {
        box = {
          width: vContent.width + parseInt(style.paddingLeft) + parseInt(style.paddingRight),
          height: vContent.height + parseInt(style.paddingTop) + parseInt(style.paddingBottom),
        };
      } else {
        console.warn('Didn\'t find a way to get a box dimensions, returning content instead');
        box = pick(vContent, ['width', 'height']);
      }
    }
    const content = pick(vContent, ['top', 'left', 'width', 'height', 'bottom', 'right']);
    if (box && content) {
      f(box, content, el);
    }
  }).observe(el);
};

/**
 * Usage:
 *   v-element-resize="(box, content, el) => { ... }"
 *   v-element-resize="({width, height}) => { ... }"
 */
export default {
  bind: initialize, //vue2
  beforeMount: initialize, //vue3
};
