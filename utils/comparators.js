export const comparators = {
  string: (a, b) => a.localeCompare(b),
  number: (a, b) => a - b,
  date: (a, b) => new Date(a) - new Date(b),
  chain: (...comparators) => (a, b) => comparators.reduce((res, cmp) =>  res || cmp(a, b), 0),
  extract: (extractor, comparator) => (a, b) => comparator(extractor(a), extractor(b)),
  reverse: comparator => (a, b) => comparator(b, a),
};
