export const generateHex = pattern => (pattern || 'xxxxxx-xxxxxx')
  .replace(/[x]/g, c => (Math.random() * 16 | 0).toString(16))
  .toLocaleUpperCase();

export default generateHex;

const chars = 'abcdefghijkmnopqrstwxyzABCDEFGHJKLMNPRTWXY346789';
export const generateRegistration = (pattern = 'xxxx-xxxx') => pattern
  .replace(/[x]/g, c => chars[(Math.random() * chars.length) | 0]);

if (typeof process !== 'undefined' && process.env.VUE_APP_ENVIRONMENT === 'local' && window) {
  window.__generateHex = generateHex;
}
